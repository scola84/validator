'use strict';

const Error = require('@scola/error');

class IntegerValidator {
  validate(value, definition) {
    value = Number(value);

    if (!Number.isInteger(value)) {
      throw new Error('validator_type_integer', {
        value
      });
    }

    if (definition.range) {
      if (value < definition.range[0]) {
        throw new Error('validator_range_left', {
          detail: {
            range: definition.range[0],
            value
          }
        });
      }

      if (value > definition.range[1]) {
        throw new Error('validator_range_right', {
          detail: {
            range: definition.range[0],
            value
          }
        });
      }
    }

    return value;
  }
}

module.exports = IntegerValidator;
