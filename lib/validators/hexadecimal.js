'use strict';

const Error = require('@scola/error');

class HexadecimalValidator {
  validate(value, definition) {
    value = String(value);

    if (!value.match(/^[0-9A-F]+$/i)) {
      throw new Error('validator_type_hexadecimal', {
        value
      });
    }

    if (definition.length) {
      if (value.length < definition.length[0]) {
        throw new Error('validator_length_left', {
          length: definition.length[0],
          value
        });
      }

      if (value.length > definition.length[1]) {
        throw new Error('validator_length_right', {
          length: definition.length[0],
          value
        });
      }
    }

    return value;
  }
}

module.exports = HexadecimalValidator;
