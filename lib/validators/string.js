'use strict';

const Error = require('@scola/error');

class StringValidator {
  validate(value, definition) {
    if (typeof value !== 'string') {
      throw new Error('validator_type_string', {
        value
      });
    }

    if (definition.length) {
      if (value.length < definition.length[0]) {
        throw new Error('validator_length_left', {
          length: definition.length[0],
          value
        });
      }

      if (value.length > definition.length[1]) {
        throw new Error('validator_length_right', {
          length: definition.length[0],
          value
        });
      }
    }

    return value;
  }
}

module.exports = StringValidator;
