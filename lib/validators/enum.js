'use strict';

const Error = require('@scola/error');

class EnumValidator {
  validate(value, definition) {
    if (definition.values.indexOf(value) === -1) {
      throw new Error('validator_type_enum', {
        detail: {
          value,
          values: definition.values
        }
      });
    }

    return value;
  }
}

module.exports = EnumValidator;
