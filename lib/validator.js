'use strict';

const Error = require('@scola/error');

class Validator {
  constructor(definitions, validators) {
    this.definitions = definitions;
    this.validators = validators;
  }

  validate(name, values) {
    if (!this.definitions[name]) {
      throw new Error('validator_definition_not_found', {
        detail: {
          name
        }
      });
    }

    const result = this.executeAll(this.definitions[name], values);

    if (result.error) {
      throw new Error('validator_invalid', {
        detail: {
          result
        }
      });
    }

    return result;
  }

  executeAll(definition, values) {
    const result = {};

    Object.keys(definition).forEach((key) => {
      if (!definition[key].type) {
        result[key] = this.executeAll(definition[key], values[key]);
        result.error = result.error || result[key].error;
        return;
      }

      try {
        result[key] = this.execute(definition[key], values[key]);
      } catch (error) {
        result[key] = error;
        result.error = true;
      }
    });

    return result.error ? result : values;
  }

  execute(definition, value) {
    if (!this.validators[definition.type]) {
      throw new Error('validator_not_found');
    }

    if (this.isEmpty(value)) {
      if (definition.required) {
        throw new Error('validator_required');
      }

      return value;
    }

    return this
      .validators[definition.type]
      .get()
      .validate(value, definition);
  }

  isEmpty(value) {
    return value === null || typeof value === 'undefined' || value === '';
  }
}

module.exports = Validator;
