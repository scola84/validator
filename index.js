'use strict';

const DI = require('@scola/di');

const I18n = require('./lib/i18n');
const Validator = require('./lib/validator');

class Module extends DI.Module {
  configure() {
    this.inject(Validator).with(
      this.object({}),
      this.object({
        enum: this.instance(require('./lib/validators/enum.js')),
        hexadecimal: this.instance(require('./lib/validators/hexadecimal.js')),
        integer: this.instance(require('./lib/validators/integer.js')),
        string: this.instance(require('./lib/validators/string.js'))
      })
    );
  }
}

module.exports = {
  I18n,
  Module,
  Validator
};
